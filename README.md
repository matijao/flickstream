Stream any movie from [Letterboxd](https://letterboxd.com/) or [IMDb](https://www.imdb.com/) in one click. Powered by [movie-web.app](https://movie-web.app/).

[![Letterboxd](https://github.com/matijaoe/flickstream/assets/46557266/8185fc8d-17fc-4417-b028-83f2a5f083f1)](https://letterboxd.com/film/asteroid-city/)
[![IMDb](https://github.com/matijaoe/flickstream/assets/46557266/c2d881f5-3cf4-428c-83ad-d44fdbb5c3aa)](https://www.imdb.com/title/tt3464902)

## Supported Browsers

- Google Chrome
- Brave Browser
- Microsoft Edge
- Mozilla Firefox

## Installation

1. Clone or download the repository

2. Open your preferred browser and navigate to the extension management page:
   - **Google Chrome**: Type `chrome://extensions` in the address bar and press Enter.
   - **Brave Browser**: Type `brave://extensions` in the address bar and press Enter.
   - **Google Chrome**: Type `edge://extensions` in the address bar and press Enter.
   - **Mozilla Firefox**: Type `about:addons` in the address bar and press Enter.

3. Enable "Developer mode" or "Load unpacked" option on the extension management page. This step may vary depending on your browser:
   - **Google Chrome**: Toggle the "Developer mode" switch in the top-right corner and click "Load unpacked". Select the extension's directory on your local machine.
   - **Brave Browser**: Toggle the "Developer mode" switch in the top-right corner and click "Load unpacked". Select the extension's directory on your local machine.
   - **Microsoft Edge**: Toggle the "Developer mode" switch in the bottom-left corner and click "Load extension". Select the extension's directory on your local machine.
   - **Mozilla Firefox**: Click on the gear icon and select "Debug Add-ons". In the new tab, click on "Load Temporary Add-on" and select the extension's directory on your local machine.

4. Your extension should now be loaded and ready to use!

## How to Use

Once the extension is successfully loaded, you can enjoy its features while browsing. Depending on the website you visit, you will see the "Watch now" button added to movie pages.

Clicking on the "Watch now" button will take you to a page where you can instantly stream the movie.

## Feedback and Contributions

If you encounter any issues, have suggestions for improvements, or would like to contribute to the development of this extension, please feel free to create an issue or submit a pull request.
