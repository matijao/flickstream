const createWatchNowButton = (movieTitle) => {
	const buttonEl = document.createElement('a')
	buttonEl.textContent = 'Watch now'

	buttonEl.href = `https://movie-web.app/search/movie/${encodeURIComponent(
		movieTitle
	)}`
	buttonEl.target = '_blank'
	buttonEl.id = 'flickstream-extension'

	return buttonEl
}

function initLetterbox() {
	const movieTitleElement = document.querySelector('h1.headline-1')
	let movieTitle = movieTitleElement?.innerText

	if (!movieTitle) {
		const alternativeTitleElement = document.querySelector(
			'h2.headline-2 span.film-title-wrapper a'
		)
		if (alternativeTitleElement) {
			movieTitle = alternativeTitleElement.innerText
		} else {
			const urlParts = window.location.pathname.split('/')
			const titleIndex = urlParts.indexOf('film') + 1
			movieTitle = urlParts[titleIndex]
		}
	}

	if (movieTitle) {
		console.log('Found', movieTitle)
	} else {
		console.error('Movie title not found')
		return
	}

	const listItem = document.createElement('li')
	listItem.setAttribute(
		'class',
		'panel-sharing sharing-toggle js-actions-panel-sharing'
	)
	listItem.style.background = '#00B021'

	const watchNowButton = createWatchNowButton(movieTitle)

	watchNowButton.style.color = 'white'
	watchNowButton.style.cursor = 'pointer'

	listItem.appendChild(watchNowButton)

	const actionsPanel = document.querySelector('ul.js-actions-panel')

	if (!actionsPanel) {
		console.error('Actions panel not found')
		return
	}

	actionsPanel.appendChild(listItem)
}

function initIMDb() {
	const movieTitleElement = document.querySelector(
		'h1[data-testid="hero__pageTitle"] > span'
	)
	let movieTitle = movieTitleElement.innerText

	if (movieTitle) {
		console.log('Found', movieTitle)
	} else {
		console.error('Movie title not found')
		return
	}

	const watchNowButton = createWatchNowButton(movieTitle)
	watchNowButton.setAttribute(
		'class',
		'ipc-btn ipc-btn--left-align-content ipc-btn--default-height ipc-btn--full-width ipc-btn--core-accent1 ipc-btn--theme-base only-button'
	)

	watchNowButton.style.height = '48px'
	watchNowButton.style.marginBottom = '12px'

	const existingContainer = document.querySelector('.bQOwSQ')
	const existingElement = existingContainer?.querySelector('.bGIoIy')

	if (!existingContainer || !existingElement) {
		console.error('Existing element not found')
		return
	}

	existingContainer.insertBefore(watchNowButton, existingElement)
}

function isLetterboxd() {
	return window.location.hostname.includes('letterboxd.com')
}

function isIMDb() {
	return window.location.hostname.includes('imdb.com')
}

if (isLetterboxd()) {
	initLetterbox()
} else if (isIMDb()) {
	initIMDb()
}
